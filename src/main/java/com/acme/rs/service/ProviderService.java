package com.acme.rs.service;

import com.acme.rs.service.dto.ProviderDTO;

import java.util.List;
import java.util.Optional;

public interface ProviderService {
    List<ProviderDTO> findAllProviders();

    Optional<ProviderDTO> findById(Long providerId);
}
