package com.acme.rs.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProductViewDTO extends ProductDTO implements Serializable {
    private List<ReviewDTO> latestReviews;
    private Integer commentCount;
    private Double voteAverage;
}
