package com.acme.rs.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProviderDTO implements Serializable {
    private Long id;
    private String name;
}
