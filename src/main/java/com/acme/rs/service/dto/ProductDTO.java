package com.acme.rs.service.dto;

import com.acme.rs.domain.enumeration.CommentAuthority;
import com.acme.rs.domain.enumeration.ViewAuthority;
import com.acme.rs.domain.enumeration.VoteAuthority;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProductDTO implements Serializable {
    private Long id;
    private String name;
    private Long providerId;
    private String providerName;
    private Boolean commentable;
    private Boolean votable;
    private CommentAuthority commentAuthority;
    private VoteAuthority voteAuthority;
    private ViewAuthority viewAuthority;
}
