package com.acme.rs.service.dto;

import com.acme.rs.domain.enumeration.ReviewState;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Data
public class ReviewDTO implements Serializable {
    private Long id;
    @NotNull
    private Long productId;
    @NotNull
    private Long userId;
    private String comment;
    private Double vote;
    private ReviewState reviewState;
    private LocalDate date;
}
