package com.acme.rs.service.impl;

import com.acme.rs.domain.Product;
import com.acme.rs.repository.ProductRepository;
import com.acme.rs.service.ProductService;
import com.acme.rs.service.ProviderService;
import com.acme.rs.service.ReviewService;
import com.acme.rs.service.dto.ProductAdministrationDTO;
import com.acme.rs.service.dto.ProductDTO;
import com.acme.rs.service.dto.ProductViewDTO;
import com.acme.rs.service.dto.ReviewDTO;
import com.acme.rs.service.exception.ReviewServiceException;
import com.acme.rs.service.mapper.ProductMapper;
import com.acme.rs.service.mapper.ProductViewMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final ProductViewMapper productViewMapper;

    private final ReviewService reviewService;

    private final ProviderService providerService;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper, ProductViewMapper productViewMapper, ReviewService reviewService, ProviderService providerService) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.productViewMapper = productViewMapper;
        this.reviewService = reviewService;
        this.providerService = providerService;
    }

    @Override
    public Optional<ProductViewDTO> findProductById(Long productId) {
        return productRepository.findById(productId).map(productViewMapper::toDTO);
    }

    @Override
    public List<ProductViewDTO> findAllProducts() {
        return productRepository.findAll().stream().map(productViewMapper::toDTO).peek(productDTO -> {
            List<ReviewDTO> reviews = new ArrayList<>();
            // It's a business decision to whether count or calculate average based on commentable and votable
            if (productDTO.getCommentable() || productDTO.getVotable()) {
                reviews = reviewService.findAllAcceptedReviewsByProductId(productDTO.getId());
            }
            if (productDTO.getCommentable()) {
                productDTO.setLatestReviews(reviewService.latestAcceptedReviewsByProductId(productDTO.getId()));
                productDTO.setCommentCount(reviews.size());
            }
            if (productDTO.getVotable()) {
                productDTO.setVoteAverage(reviews.stream().map(ReviewDTO::getVote)
                        .mapToDouble(Double::doubleValue).average().orElse(0.0));
            }
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ProductViewDTO addProduct(ProductDTO productDTO) {
        if (productDTO.getId() != null) {
            throw new ReviewServiceException("New product cannot have id.");
        }
        providerService.findById(productDTO.getProviderId())
                .orElseThrow(() -> new ReviewServiceException("Provider does not exit"));

        return productViewMapper.toDTO(productRepository.save(productMapper.toEntity(productDTO)));
    }

    @Override
    @Transactional
    public void updateProductAdministrativeFeatures(Long productId, ProductAdministrationDTO productAdministrationDTO) {
        if (productId == null) {
            throw new ReviewServiceException("Product id not specified.");
        }
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ReviewServiceException("Product does not exist."));
        if (productAdministrationDTO.getCommentable() != null) {
            product.setCommentable(productAdministrationDTO.getCommentable());
        }
        if (productAdministrationDTO.getVotable() != null) {
            product.setVotable(productAdministrationDTO.getVotable());
        }
        if (productAdministrationDTO.getCommentAuthority() != null) {
            product.setCommentAuthority(productAdministrationDTO.getCommentAuthority());
        }
        if (productAdministrationDTO.getVoteAuthority() != null) {
            product.setVoteAuthority(productAdministrationDTO.getVoteAuthority());
        }
        if (productAdministrationDTO.getViewAuthority() != null) {
            product.setViewAuthority(productAdministrationDTO.getViewAuthority());
        }
        productRepository.save(product);
    }

}
