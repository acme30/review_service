package com.acme.rs.service.impl;

import com.acme.rs.domain.Review;
import com.acme.rs.domain.enumeration.ReviewState;
import com.acme.rs.repository.ReviewRepository;
import com.acme.rs.service.ProductService;
import com.acme.rs.service.ReviewService;
import com.acme.rs.service.dto.ProductViewDTO;
import com.acme.rs.service.dto.ReviewDTO;
import com.acme.rs.service.exception.ReviewServiceException;
import com.acme.rs.service.mapper.ReviewMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;

    private final ReviewMapper reviewMapper;

    private final ProductService productService;

    public ReviewServiceImpl(ReviewRepository reviewRepository, ReviewMapper reviewMapper, @Lazy ProductService productService) {
        this.reviewRepository = reviewRepository;
        this.reviewMapper = reviewMapper;
        this.productService = productService;
    }

    @Override
    public List<ReviewDTO> findAllReviews() {
        return reviewRepository.findAll().stream().map(reviewMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<ReviewDTO> latestAcceptedReviewsByProductId(Long productId) {
        return reviewRepository.findTop3ByProduct_IdAndReviewStateOrderByDateDesc(productId, ReviewState.ACCEPTED)
                .stream().map(reviewMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<ReviewDTO> findAllAcceptedReviewsByProductId(Long productId) {
        return reviewRepository.findAllByProduct_IdAndReviewState(productId, ReviewState.ACCEPTED)
                .stream().map(reviewMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public ReviewDTO addReview(ReviewDTO reviewDTO) {
        if (reviewDTO.getId() != null) {
            throw new RuntimeException("New review can't have id.");
        }
        ProductViewDTO product = productService.findProductById(reviewDTO.getProductId())
                .orElseThrow(() -> new ReviewServiceException("Product does not exist."));
        if (!product.getCommentable() && reviewDTO.getComment() != null) {
            throw new RuntimeException("Product is not commentable.");
        }
        if (!product.getVotable() && reviewDTO.getVote() != null) {
            throw new RuntimeException("Product is not votable.");
        }

        Review review = reviewMapper.toEntity(reviewDTO);
        review.setReviewState(ReviewState.PENDING);
        review.setDate(LocalDate.now());
        review = reviewRepository.save(review);
        return reviewMapper.toDTO(review);
    }

    @Override
    @Transactional
    public void updateReviewState(Long reviewId, ReviewState reviewState) {
        reviewRepository.updateReviewState(reviewId, reviewState);
    }
}
