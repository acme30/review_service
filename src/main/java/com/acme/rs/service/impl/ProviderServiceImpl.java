package com.acme.rs.service.impl;

import com.acme.rs.repository.ProviderRepository;
import com.acme.rs.service.ProviderService;
import com.acme.rs.service.dto.ProviderDTO;
import com.acme.rs.service.mapper.ProviderMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProviderServiceImpl implements ProviderService {

    private final ProviderRepository providerRepository;

    private final ProviderMapper providerMapper;

    public ProviderServiceImpl(ProviderRepository providerRepository, ProviderMapper providerMapper) {
        this.providerRepository = providerRepository;
        this.providerMapper = providerMapper;
    }

    @Override
    public List<ProviderDTO> findAllProviders() {
        return providerRepository.findAll().stream().map(providerMapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public Optional<ProviderDTO> findById(Long providerId) {
        return providerRepository.findById(providerId).map(providerMapper::toDTO);
    }
}
