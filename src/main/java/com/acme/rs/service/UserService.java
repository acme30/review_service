package com.acme.rs.service;

import com.acme.rs.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAllUsers();
}
