package com.acme.rs.service.exception;

public class ReviewServiceException extends RuntimeException {
    public ReviewServiceException() {
    }

    public ReviewServiceException(String message) {
        super(message);
    }
}
