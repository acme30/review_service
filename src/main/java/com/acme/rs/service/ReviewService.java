package com.acme.rs.service;

import com.acme.rs.domain.enumeration.ReviewState;
import com.acme.rs.service.dto.ReviewDTO;

import java.util.List;

public interface ReviewService {
    List<ReviewDTO> findAllReviews();

    List<ReviewDTO> latestAcceptedReviewsByProductId(Long productId);

    List<ReviewDTO> findAllAcceptedReviewsByProductId(Long productId);

    ReviewDTO addReview(ReviewDTO reviewDTO);

    void updateReviewState(Long reviewId, ReviewState reviewState);
}
