package com.acme.rs.service.mapper;

import com.acme.rs.domain.Product;
import com.acme.rs.service.dto.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProductMapper extends EntityMapper<Product, ProductDTO> {

    @Override
    @Mapping(source = "providerId", target = "provider.id")
    @Mapping(source = "providerName", target = "provider.name")
    Product toEntity(ProductDTO productDTO);

    @Override
    @Mapping(source = "provider.id", target = "providerId")
    @Mapping(source = "provider.name", target = "providerName")
    ProductDTO toDTO(Product product);
}
