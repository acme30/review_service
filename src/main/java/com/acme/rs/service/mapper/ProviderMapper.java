package com.acme.rs.service.mapper;

import com.acme.rs.domain.Provider;
import com.acme.rs.service.dto.ProviderDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProviderMapper extends EntityMapper<Provider, ProviderDTO> {
}
