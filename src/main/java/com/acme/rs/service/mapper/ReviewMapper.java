package com.acme.rs.service.mapper;

import com.acme.rs.domain.Review;
import com.acme.rs.service.dto.ReviewDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReviewMapper extends EntityMapper<Review, ReviewDTO> {

    @Override
    @Mapping(source = "productId", target = "product.id")
    @Mapping(source = "userId", target = "user.id")
    Review toEntity(ReviewDTO reviewDTO);

    @Override
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "user.id", target = "userId")
    ReviewDTO toDTO(Review review);
}
