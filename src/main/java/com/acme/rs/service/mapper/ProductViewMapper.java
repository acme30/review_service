package com.acme.rs.service.mapper;

import com.acme.rs.domain.Product;
import com.acme.rs.service.dto.ProductViewDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProductViewMapper extends EntityMapper<Product, ProductViewDTO> {

    @Override
    @Mapping(source = "providerId", target = "provider.id")
    @Mapping(source = "providerName", target = "provider.name")
    Product toEntity(ProductViewDTO productViewDTO);

    @Override
    @Mapping(source = "provider.id", target = "providerId")
    @Mapping(source = "provider.name", target = "providerName")
    ProductViewDTO toDTO(Product product);
}
