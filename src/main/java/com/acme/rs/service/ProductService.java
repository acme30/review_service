package com.acme.rs.service;

import com.acme.rs.service.dto.ProductAdministrationDTO;
import com.acme.rs.service.dto.ProductDTO;
import com.acme.rs.service.dto.ProductViewDTO;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    Optional<ProductViewDTO> findProductById(Long productId);

    List<ProductViewDTO> findAllProducts();

    ProductViewDTO addProduct(ProductDTO productDTO);

    void updateProductAdministrativeFeatures(Long productId, ProductAdministrationDTO productAdministrationDTO);
}
