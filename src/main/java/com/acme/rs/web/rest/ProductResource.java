package com.acme.rs.web.rest;

import com.acme.rs.service.ProductService;
import com.acme.rs.service.dto.ProductAdministrationDTO;
import com.acme.rs.service.dto.ProductDTO;
import com.acme.rs.service.dto.ProductViewDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductResource {

    private final ProductService productService;

    public ProductResource(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<ProductViewDTO> getAllProducts() {
        return productService.findAllProducts();
    }

    @PostMapping
    public ResponseEntity<ProductViewDTO> addNewProduct(@RequestBody ProductDTO productDTO) {
        return ResponseEntity.ok(productService.addProduct(productDTO));
    }

    @PutMapping("/administration/{productId}")
    public ResponseEntity<Void> updateProductAdministrativeFeatures(
            @PathVariable Long productId, @RequestBody ProductAdministrationDTO productAdministrationDTO) {
        productService.updateProductAdministrativeFeatures(productId, productAdministrationDTO);
        return ResponseEntity.ok().build();
    }
}
