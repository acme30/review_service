package com.acme.rs.web.rest;

import com.acme.rs.service.ProviderService;
import com.acme.rs.service.dto.ProviderDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/providers")
public class ProviderResource {

    private final ProviderService providerService;

    public ProviderResource(ProviderService providerService) {
        this.providerService = providerService;
    }

    @GetMapping
    public List<ProviderDTO> getAllProviders() {
        return providerService.findAllProviders();
    }
}

