package com.acme.rs.web.rest;

import com.acme.rs.domain.enumeration.ReviewState;
import com.acme.rs.service.ReviewService;
import com.acme.rs.service.dto.ReviewDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/reviews")
public class ReviewResource {

    private final ReviewService reviewService;

    public ReviewResource(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping
    public List<ReviewDTO> getAllReviews() {
        return reviewService.findAllReviews();
    }

    @PostMapping
    public ResponseEntity<ReviewDTO> addReview(@Valid @RequestBody ReviewDTO reviewDTO) {
        return ResponseEntity.ok(reviewService.addReview(reviewDTO));
    }

    @PutMapping("/{reviewId}/{reviewState}")
    public ResponseEntity<Void> updateReviewState(@PathVariable Long reviewId, @PathVariable ReviewState reviewState) {
        reviewService.updateReviewState(reviewId, reviewState);
        return ResponseEntity.ok().build();
    }
}
