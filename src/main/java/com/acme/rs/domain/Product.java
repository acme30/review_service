package com.acme.rs.domain;

import com.acme.rs.domain.enumeration.CommentAuthority;
import com.acme.rs.domain.enumeration.ViewAuthority;
import com.acme.rs.domain.enumeration.VoteAuthority;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "provider_id", referencedColumnName = "id")
    private Provider provider;

    private Boolean commentable;

    private Boolean votable;

    @Enumerated(EnumType.STRING)
    private CommentAuthority commentAuthority;

    @Enumerated(EnumType.STRING)
    private VoteAuthority voteAuthority;

    @Enumerated(EnumType.STRING)
    private ViewAuthority viewAuthority;
}
