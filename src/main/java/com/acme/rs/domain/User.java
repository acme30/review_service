package com.acme.rs.domain;

import com.acme.rs.domain.enumeration.UserType;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "RS_USER")
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String username;

    @Enumerated(EnumType.STRING)
    private UserType userType;
}
