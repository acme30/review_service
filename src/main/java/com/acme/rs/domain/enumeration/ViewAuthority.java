package com.acme.rs.domain.enumeration;

public enum ViewAuthority {
    PUBLIC, CUSTOMER
}
