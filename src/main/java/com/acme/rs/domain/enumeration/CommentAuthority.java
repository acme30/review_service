package com.acme.rs.domain.enumeration;

public enum CommentAuthority {
    PUBLIC, CUSTOMER
}
