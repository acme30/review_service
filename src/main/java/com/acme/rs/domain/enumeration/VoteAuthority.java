package com.acme.rs.domain.enumeration;

public enum VoteAuthority {
    PUBLIC, CUSTOMER
}
