package com.acme.rs.domain.enumeration;

public enum UserType {
    PRODUCT_MANAGER, NORMAL_USER
}
