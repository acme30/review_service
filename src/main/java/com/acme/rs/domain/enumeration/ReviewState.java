package com.acme.rs.domain.enumeration;

public enum ReviewState {
    PENDING, ACCEPTED, REJECTED
}
