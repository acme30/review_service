package com.acme.rs.domain;

import com.acme.rs.domain.enumeration.ReviewState;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Product product;

    @ManyToOne
    private User user;

    private String comment;

    private Double vote;

    @Enumerated(EnumType.STRING)
    private ReviewState reviewState;

    private LocalDate date;

}
