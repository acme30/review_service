package com.acme.rs.repository;

import com.acme.rs.domain.Review;
import com.acme.rs.domain.enumeration.ReviewState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    List<Review> findTop3ByProduct_IdAndReviewStateOrderByDateDesc(Long productId, ReviewState reviewState);

    List<Review> findAllByProduct_IdAndReviewState(Long productId, ReviewState reviewState);

    @Modifying
    @Query("update Review review set review.reviewState= :reviewState where review.id = :reviewId")
    void updateReviewState(Long reviewId, ReviewState reviewState);
}
