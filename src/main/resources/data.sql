INSERT INTO provider (name)
VALUES ('Acme Corporation');

INSERT INTO product (name, provider_id, commentable, votable, comment_authority, vote_authority, view_authority)
VALUES ('Matches', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Street Cleaners Wagon', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Bird Seed Ver. 1', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Bird Seed Ver. 2', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Triple-Strength Fortified Leg Muscle Vitamins', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Glue V1', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Artificial Rock', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Grease', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Giant Rubber Band V1', 1, true, true, 'PUBLIC', 'PUBLIC', 'PUBLIC'),
       ('Roller Skis', 1, true, true, 'PUBLIC', 'CUSTOMER', 'PUBLIC'),
       ('Invisible Paint', 1, true, true, 'CUSTOMER', 'CUSTOMER', 'PUBLIC');


INSERT INTO rs_user(name, username, user_type)
VALUES ('Road Runner', 'road_runner', 'PRODUCT_MANAGER'),
       ('Wile E. Coyote', 'coyote', 'NORMAL_USER');

INSERT INTO review(product_id, user_id, comment, vote, review_state, date)
VALUES (3, 2, 'At least I did not starve!', 3.5, 'ACCEPTED', '2022-08-04'),
       (4, 2, 'Better than Ver. 1, but all eaten by somebody else', 4.5, 'PENDING', '2022-08-04'),
       (11, 2, 'Finally I could catch road runner with this invisible paint', 5.0, 'REJECTED', '2022-08-04');